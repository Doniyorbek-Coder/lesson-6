study = document.querySelector(".study");
items = document.querySelectorAll(".study__item");
active = document.getElementsByClassName("study__item--active");

items.forEach((element) => {
  element.addEventListener("click", function () {
    bgColor = element.children[1].getAttribute("data-bgColor");
    study.style.backgroundColor = bgColor;
    active[0].className = active[0].className.replace(
      " study__item--active",
      ""
    );
    this.className += " study__item--active";
  });
});
